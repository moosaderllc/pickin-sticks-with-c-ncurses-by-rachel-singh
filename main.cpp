#include <ncurses.h>
#include <cstdlib>
#include <ctime>

// Tutorial: http://www.tldp.org/HOWTO/NCURSES-Programming-HOWTO/init.html

// COLOR_BLACK, COLOR_RED, COLOR_GREEN, COLOR_YELLOW, COLOR_BLUE, COLOR_MAGENTA, COLOR_CYAN, COLOR_WHITE

void SetupCurses()
{
	initscr();		// Start curses mode
	raw();			// Line buffering disabled
	keypad( stdscr, TRUE );	// F1, F2, ...
	noecho();		// Don't echo while getch
	start_color();		// Colors!

	// Init colors
	init_pair( 1, COLOR_GREEN, COLOR_GREEN );	// Grass
	init_pair( 2, COLOR_RED, COLOR_GREEN );		// Stick
	init_pair( 3, COLOR_BLACK, COLOR_GREEN );	// Player
}

void TeardownCurses()
{
	endwin();	// End
}

void PaintGrass()
{
	int grass = 'x';

	attron( COLOR_PAIR( 1 ) );
	for ( int y = 0; y < 25; y++ )
	{
		for ( int x = 0; x < 80; x++ )
		{
			mvaddch( y, x, grass );
		}
	}
	attroff( COLOR_PAIR( 1 ) );
}

void PaintItem( int x, int y, int colorCode, int symbol )
{
	attron( COLOR_PAIR( colorCode ) );
	mvaddch( y, x, symbol );
	attroff( COLOR_PAIR( colorCode ) );
}

int GetInput()
{
	int input = getch();
	return input;
}

void HandleMove( int input, int& playerX, int& playerY )
{
	if 	( input == KEY_LEFT )
	{
		playerX -= 1;
	}
	else if ( input == KEY_RIGHT )
	{
		playerX += 1;
	}
	else if ( input == KEY_DOWN )
	{
		playerY += 1;
	}
	else if ( input == KEY_UP )
	{
		playerY -= 1;
	}

	if ( playerX < 0 ) { playerX = 0; }
	else if ( playerX > 79 ) { playerX = 79; }
	if ( playerY < 0 ) { playerY = 0; }
	else if ( playerY > 24 ) { playerY = 24; }
}

void RefreshScreen()
{
	refresh();
}

bool IsCollision( int sx, int sy, int px, int py )
{
	return ( px == sx && py == sy );
}

void SetRandomCoordinates( int& x, int& y )
{
	x = rand() % 80;
	y = rand() % 23;
}

void PrintGameInfo( int score )
{
	mvprintw( 23, 0, "Q = Quit" );
	mvprintw( 24, 0, "Score: %d", score );
}

int main()
{
	SetupCurses();
	srand( time( NULL ) );

	int stickX, stickY;
	SetRandomCoordinates( stickX, stickY );
	int playerX = 5, playerY = 6;
	int input;
	int score = 0;

	int stickIcon = 'Y';
	int playerIcon = '@';

	bool done = false;

	while ( !done )
	{
		PaintGrass();

		PaintItem( stickX, stickY, 2, stickIcon );
		PaintItem( playerX, playerY, 3, playerIcon );

		PrintGameInfo( score );
 		input = GetInput();

		HandleMove( input, playerX, playerY );

		if ( IsCollision( stickX, stickY, playerX, playerY ) )
		{
			score += 1;
			SetRandomCoordinates( stickX, stickY );
		}

		if ( score == 10 )
		{
			done = true;
			mvprintw( 10, 40, "YOU WIN!" );
		} 
		if ( input == 'Q' || input == 'q' )
		{
			done = true;
			mvprintw( 10, 40, "GOODBYE!" );
		}

		RefreshScreen();
	}

	getch();

	TeardownCurses();

	return 0;
}
